import math
from random import randint
from pyparsing import *
from Constnants import LOGO_COMMANDS, UNARY, BINARY, SENSOR, LETTERS, DIGITS



class Parser:
    def __init__(self, plot):
        self.letters = LETTERS + LETTERS.lower()
        self.digits = DIGITS
        self.letdig = self.letters + self.digits
        self.all_symbols = self.letdig + printables
        self.variables = {}
        self.procedures = {}
        self.current_proc = ''
        self.active = []
        self.plot = plot

    def proc_to_dict(self, back_script):
        """создает словарь процедур c  параметрами (если есть)"""

        proc = list(re.findall(r"(?P<def>(?<=это).+(?=\n+))(?P<body>(.*?\n*?)*?)(?=конец)", back_script))

        if proc:
            for p in proc:
                proc_declare = p[0].replace(' ', '').split(":")
                proc_name = proc_declare.pop(0)
                proc_vars = proc_declare
                proc_body = p[1].replace("\n", ' ')
                self.procedures[proc_name] = [proc_vars, proc_body]
        return

    def parser(self, front_script):
        """ преобразует строку команд в список вида ['вп', '100'] """

        proc_in_dict = ' '.join(self.procedures.keys())

        # грамматика
        name = Word(self.letdig + "_")
        set_var = Word("\"").suppress() + name
        read_var = Word(":") + name

        parens = nestedExpr('[', ']', ignoreExpr=None).setParseAction(lambda t: str('[' + ' '.join(t[0]) + ']'))

        expression = Forward()

        lst = nestedExpr('[', ']', ignoreExpr=None).setParseAction(lambda t: str(' '.join(t[0])))

        expression << Group(
            (Word(self.digits + " +-*/^()@<>=.") ^ read_var ^ (oneOf(BINARY) + Optional(lst)) ^ oneOf(UNARY) ^ oneOf(
                SENSOR))).setParseAction(lambda t: ' '.join(t[0]))

        statement = OneOrMore(Group(((oneOf(LOGO_COMMANDS) | oneOf(proc_in_dict)) + Optional(set_var) + \
                                     Optional(Group(OneOrMore(expression)).setParseAction(lambda t: ' '.join(t[0]))) + \
                                     Optional(OneOrMore(parens)))))

        try:
            parsed = statement.parseString(front_script)
            print('parsed =', parsed)
            return parsed
        except (ParseException, UnboundLocalError):
            print('нет такой команды')

    # считаем выражения
    def exp_evaluate(self, input_string):
        # Debugging flag can be set to either "debug_flag=True" or "debug_flag=False"
        debug_flag = False
        # print("input_string = ", input_string)

        exprStack = []
        varStack = []

        try:
            local_vars = self.current_proc[0]
        except:
            local_vars = {}

        input_string = input_string.replace("<", "@")  # pyparsing не понимает знака "<" - пришлось заменить :-(

        def pushFirst(str, loc, toks):
            exprStack.append(toks[0])

        def assignVar(str, loc, toks):
            varStack.append(toks[0])

        # define grammar
        point = Literal('.')
        e = CaselessLiteral('E')
        plusorminus = Literal('+') | Literal('-')
        number = Word(nums)
        integer = Combine(Optional(plusorminus) + number)
        floatnumber = Combine(integer +
                              Optional(point + Optional(number)) +
                              Optional(e + integer)
                              )

        var_name = Suppress(':') + Word(self.letters, self.letdig + '_')

        plus = Literal("+")
        minus = Literal("-")
        mult = Literal("*")
        div = Literal("/")
        gt = Literal(">")
        lt = Literal("@")
        eq = Literal("=")
        lpar = Literal("(").suppress()
        rpar = Literal(")").suppress()
        addop = plus | minus
        compare = gt | lt | eq
        multop = mult | div
        expop = Literal("^")

        expr = Forward()

        params = nestedExpr('[', ']', ignoreExpr=None).setParseAction(lambda t: str('[' + ' '.join(t[0]) + ']'))

        unary = (oneOf(UNARY) + expr)
        binary = oneOf(BINARY) + (OneOrMore(expr) | params)
        sensor = (oneOf(SENSOR) + ZeroOrMore(expr))

        atom = ((e | floatnumber | integer | var_name | unary | binary | sensor).setParseAction(pushFirst) |
                (lpar + expr.suppress() + rpar))

        factor = Forward()
        factor << atom + ZeroOrMore((expop + factor).setParseAction(pushFirst))

        term = factor + ZeroOrMore((multop + factor).setParseAction(pushFirst))
        expr << term + ZeroOrMore((addop + term).setParseAction(pushFirst))

        logic = Forward()
        logic << expr + ZeroOrMore((compare + logic).setParseAction(pushFirst))

        pattern = logic + StringEnd()

        # map operator symbols to corresponding arithmetic operations
        opn = {"+": (lambda a, b: a + b),
               "-": (lambda a, b: a - b),
               "*": (lambda a, b: a * b),
               "/": (lambda a, b: a / b),
               ">": (lambda a, b: a > b),
               "@": (lambda a, b: a < b),  # pyparsing не понимает знака "<" - пришлось заменить :-(
               "=": (lambda a, b: a == b),
               "^": (lambda a, b: a ** b),
               }

        opn_unary = {"sin": (lambda x: math.sin(math.radians(x))),
                     "cos": (lambda x: math.cos(math.radians(x))),
                     "arctg": (lambda x: math.degrees(math.atan(x))),
                     "сл": (lambda x: randint(0, x - 1)),
                     "оч": (lambda x: round(x)),
                     "целое": (lambda x: math.floor(x)),
                     "кк": (lambda x: math.sqrt(x)),
                     }

        opn_binary = {"остаток": (lambda a, b: a % b),
                      "доточки": (lambda x, y: (math.hypot(x - self.plot.active[0].current_coord[0],
                                                           y + self.plot.active[0].current_coord[1]))),
                      "угол": (lambda x, y: (360 + (math.degrees(math.atan2(x - self.plot.active[0].current_coord[0],
                                                                            y + self.plot.active[0].current_coord[
                                                                                1])))) % 360),
                      }

        opn_sensor = {"х": (lambda: self.plot.active[0].current_coord[0]),
                      "у": (lambda: -self.plot.active[0].current_coord[1]),
                      "место": (lambda: str(self.plot.active[0].current_coord[0]) +
                                        ' ' + str(-self.plot.active[0].current_coord[1])),
                      "курс": (lambda: (360 + self.plot.active[0].current_angle) % 360),
                      "цвет": (lambda: self.plot.getPenColor()),
                      "цп": (lambda: self.plot.getFieldColor()),
                      "цт": (lambda: self.plot.getTextColor()),
                      "фон": (lambda: self.plot.getBackgroundColor()),
                      }

        # Recursive function that evaluates the stack
        def evaluateStack(s):
            op = s.pop()
            if op in "+-*/^@<>=":
                op2 = evaluateStack(s)
                op1 = evaluateStack(s)
                return opn[op](op1, op2)
            elif op in opn_unary:
                op1 = evaluateStack(s)
                return opn_unary[op](op1)
            elif op in opn_sensor:
                return opn_sensor[op]()
            elif op in opn_binary:
                if len(s) == 3:  # костыль, если второй аргумент отрицательный
                    s[1] = str(int(s[1]) * (-1))
                    del s[2]
                op2 = evaluateStack(s)
                op1 = evaluateStack(s)
                return opn_binary[op](op1, op2)
            elif op in local_vars:
                return local_vars[op]
            elif op in self.variables:
                return self.variables[op]
            elif re.search('^[-+]?[0-9]+$', op):
                return int(op)
            else:
                return float(op)

        try:
            L = pattern.parseString(input_string)
        except ParseException as err:
            L = ['Parse Failure', input_string]

        # show evalResult of parsing the input string
        if debug_flag: print(input_string, "->", L)
        if len(L) == 0 or L[0] != 'Parse Failure':
            if debug_flag: print("exprStack=", exprStack)

            # calculate evalResult , store a coкpy in ans , display the evalResult to user
            evalResult = evaluateStack(exprStack)

            # Assign evalResult to a variable if required
            if debug_flag: print("var=", varStack)
            if len(varStack) == 1:
                var_res = varStack.pop()
                if var_res in local_vars:
                    local_vars[var_res] = evalResult
                if var_res in self.variables:
                    self.variables[var_res] = evalResult
            if debug_flag: print("variables=", self.variables)

            return evalResult

