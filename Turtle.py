
from PyQt5 import QtCore, QtGui, QtWidgets
from Constnants import COLORS

t_catalog = './turtles/'


class Turtle(QtWidgets.QGraphicsPixmapItem):
    def __init__(self, parent=None, angle=0, active=True, show=True, color=COLORS[1],
                 image=t_catalog + '1.png', coord=[0, 0]):
        super(Turtle, self).__init__(parent)

        self.form = QtGui.QPixmap(image)
        self.setPixmap(self.form)
        self.setTransformationMode(QtCore.Qt.SmoothTransformation)

        self.active = active
        self.show = show

        self.current_angle = angle
        self.current_coord = coord
        self.setZValue(1)

        self.pen = QtGui.QPen()
        self.pen_color = QtGui.QColor(color)
        self.pen_state = QtCore.Qt.SolidLine

        self.setTurtleAnglePos()
        self.setTurtleColor(color)

    def setTurtleAnglePos(self, angle=0):
        self.current_angle = angle
        x, y = self.current_coord
        self.setPos(x - 16, y - 16)

        self.setTransformOriginPoint(self.boundingRect().center())
        self.setRotation(self.current_angle)
        self.resetTransform()

    def setTurtleColor(self, colour):
        self.pen_color = QtGui.QColor(colour)
        self.pen.setColor(self.pen_color)

        # Цветовая метка
        # paintdevice = self.pixmap()
        # painter = QtGui.QPainter(paintdevice)
        # painter.setBrush(QtGui.QBrush(QtGui.QColor(self.pen_color)))
        # painter.drawEllipse(3, 3, 7, 7)
        # painter.end()
        # self.setPixmap(paintdevice)

        image = QtGui.QImage(self.pixmap())
        for x in range(image.height()):
            for y in range(image.width()):
                if image.pixelColor(x, y) == QtCore.Qt.transparent:
                    continue
                else:
                    image.setPixelColor(x, y, QtGui.QColor(self.pen_color))
        self.setPixmap(QtGui.QPixmap(image))
        self.form = QtGui.QPixmap(image)

    def setTurtleForm(self, pixmap):
        self.setPixmap(pixmap)
        self.form = QtGui.QPixmap(pixmap)
        self.setTurtleColor(self.pen_color)
