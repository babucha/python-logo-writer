
from PyQt5 import QtWidgets


class ScriptEnter(QtWidgets.QWidget):
    def __init__(self, plot):
        super(ScriptEnter, self).__init__()
        self.setGeometry(150, 150, 400, 400)
        self.plot = plot
        self.setWindowTitle("Ввод программы LOGO")
        self.front = QtWidgets.QTextEdit(self)
        self.back = QtWidgets.QTextEdit(self)

        self.btn1 = QtWidgets.QPushButton("Нарисовать!", self)
        self.btn2 = QtWidgets.QPushButton("Test 1", self)
        self.btn3 = QtWidgets.QPushButton("Test 2", self)

        self.btn1.setDefault(True)

        self.label_f = QtWidgets.QLabel("Командная строка")
        self.label_b = QtWidgets.QLabel("Изнанка")

        self.hbox_front = QtWidgets.QHBoxLayout()
        self.hbox_front.addWidget(self.btn1)
        self.hbox_front.addWidget(self.btn2)
        self.hbox_front.addWidget(self.btn3)

        self.vbox = QtWidgets.QVBoxLayout()

        self.vbox.addWidget(self.label_f)
        self.vbox.addWidget(self.front)
        self.vbox.addLayout(self.hbox_front)
        self.vbox.addWidget(self.label_b)
        self.vbox.addWidget(self.back)

        self.setLayout(self.vbox)

        self.btn1.clicked.connect(self.front_textEvent)
        self.btn2.clicked.connect(self.test_script1)
        self.btn3.clicked.connect(self.test_script2)

        self.front_script = ""
        self.back_script = ""

    def front_textEvent(self, event):
        if self.front.toPlainText():
            self.plot.execute(self.front.toPlainText(), self.back.toPlainText())
        else:
            print('\nВведите программу!')

    def test_script1(self, event):

        debug_script = 'иг пусть "а 2 повтори 12 ' \
                       '[нц :а вп 60 пр 30 пусть "а :а+1]  ' \
                       'пр 55 пп вп 100 по нц 4 повтори 4 [вп 10 пр 90] ' \
                       'пп нд 30 по нц 5 крась заполни'

        debug_script = 'иг пиши фон'

        debug_script = 'иг пусть "а 0 пусть "в 10 повтори 4 ' \
                       '[для сл 4 нц :а+сл 5 лв :в  пч вп сл 160 пусть "а :а+1  ' \
                       'пусть "в :в+30] пусть "м 5 повтори :м+1 [вп сл 80 пр 360/(:м+1)]'

        self.front.setText(debug_script)
        self.plot.execute(debug_script)

    def test_script2(self, event):

        debug_script = 'иг пусть "а 2 повтори 12 [нц :а вп 60 пр 30 пусть "а :а+1]  ' \
                       'пр 55 пп вп 100 по нц 4 повтори 4 [вп 10 пр 90] пп нд 30 по нц 5 ' \
                       'крась нц 15 заполни новфон 10 сч'
        self.front.setText(debug_script)
        self.plot.execute(debug_script)
