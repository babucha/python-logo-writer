
import time
import math
from PyQt5 import QtCore, QtGui, QtWidgets
from Turtle import Turtle
from MyText import MyText
from Parser import Parser
from pyparsing import nestedExpr
from Constnants import COLORS


t_catalog = './turtles/'


def safe_evaluate(func):
    def func_wrapper(*args):
        try:
            results = func(*args)
        except IndexError as e:
            if len(args) >= 2:
                print("Неверный синтаксис в команде {0}".format(args[1].upper()))
            else:
                print("ОШИБКА! {0}".format(e))

            return None
        except ValueError as ve:
            print(ve)
            return None

        return results
    return func_wrapper


class Plotter(QtWidgets.QGraphicsView):
    def __init__(self, app):

        super(Plotter, self).__init__()

        self.app = app
        self.scene = QtWidgets.QGraphicsScene()
        self.setScene(self.scene)
        self.scene.setSceneRect(-320, -190, 640, 380)
        self.active = []
        self.turtles = []
        self.parser = Parser(self)

        self.initGraphics()
        self.initText()

    def initGraphics(self):

        self.scene.setBackgroundBrush(QtGui.QColor(COLORS[0]))

        items = [i for i in self.scene.items() if not isinstance(i, MyText)]
        for i in items:
            self.scene.removeItem(i)

        self.turtles = []
        self.turtles.append(Turtle(angle=0, active=True, show=True, image=t_catalog + '1.png', coord=[0, 0]))
        self.turtles.append(Turtle(angle=0, active=False, show=False, image=t_catalog + '1.png', coord=[0, 80]))
        self.turtles.append(Turtle(angle=0, active=False, show=False, image=t_catalog + '1.png', coord=[-80, 80]))
        self.turtles.append(Turtle(angle=0, active=False, show=False, image=t_catalog + '1.png', coord=[-80, 0]))

        self.active = [t for t in self.turtles if t.active]
        self.parser.active = self.active

        for t in self.turtles:
            self.scene.addItem(t)
            t.setVisible(t.show)

    def initText(self):
        txt = [t for t in self.scene.items() if isinstance(t, MyText)]
        for t in txt:
            self.scene.removeItem(t)
        self.text_coord = [-320, -190]
        self.text_cursor = 0
        self.text_colour = QtGui.QColor(COLORS[1])

    @safe_evaluate
    def execute(self, front_script, back_script=''):

        if back_script:
            self.parser.proc_to_dict(back_script)

        parsed_script = self.parser.parser(front_script)

        if not parsed_script:
            print('Введите корректную программу')
            return

        for c in parsed_script:

            # запуск процедуры и присвоение локальных переменных
            if c[0] in self.parser.procedures.keys():  # если команда - это внешняя процедура
                self.parser.current_proc = self.parser.procedures[c[0]]
                args = self.parser.current_proc[0]
                if args:
                    local_vars = dict(zip(args, map(self.parser.exp_evaluate, c[1].split(' '))))
                    self.parser.current_proc[0] = local_vars  # присваиваем локальные переменные
                self.execute(self.parser.current_proc[1])  # исполняем процедуру
                self.parser.current_proc = ''  # удаляем исполненную процедуру из буфера со всеми ее локльными переменными

            elif c[0] == 'нц':
                self.setPenColor(COLORS[int(self.parser.exp_evaluate(c[1])) % 16])
            elif c[0] == 'нцт':
                self.setTextColor(COLORS[int(self.parser.exp_evaluate(c[1])) % 16])

            elif c[0] == 'нф':
                self.setTurtleForm(t_catalog + str(self.parser.exp_evaluate(c[1])) + '.png')

            elif c[0] == 'пп':
                self.setPenState(QtCore.Qt.NoPen)
            elif c[0] == 'по':
                self.setPenState(QtCore.Qt.SolidLine)

            elif c[0] == 'крась':
                self.floodFill(fill=True)
            elif c[0] == 'заполни':
                self.floodFill(pattern=True)
            elif c[0] == 'новфон' or c[0] == 'нф':
                self.setBackground(self.parser.exp_evaluate(c[1]) % 16)

            elif c[0] == 'сч':
                self.showTurtle(False)
            elif c[0] == 'пч':
                self.showTurtle(True)

            elif c[0] == 'вп':
                radius = float(self.parser.exp_evaluate(c[1]))
                self.draw(radius, direction=0)
            elif c[0] == 'нд':
                radius = float(self.parser.exp_evaluate(c[1]))
                self.draw(radius, direction=-180)

            elif c[0] == 'пиши':
                try:
                    text = (self.parser.exp_evaluate(c[1]))
                    self.write(str(int(text)))
                except IndexError:
                    print('неверный аргумент команды ПИШИ')
                    return
                except:
                    text = str(c[1])
                    self.write(text)

            elif c[0] == 'покажи':

                # не работает

                try:
                    text = str(self.parser.exp_evaluate(c[1]))
                except:
                    text = str(c[1])
                text = self.app.main.script_enter.front.toPlainText() + "\n" + text
                self.app.main.script_enter.front.setText(text)

            elif c[0] == 'пр':
                ang = float(self.parser.exp_evaluate(c[1]))
                self.turn(ang)
            elif c[0] == 'лв':
                ang = -float(self.parser.exp_evaluate(c[1]))
                self.turn(ang)

            elif c[0] == 'нк' or c[0] == 'новкурс':
                for t in self.active:
                    t.setTurtleAnglePos(self.parser.exp_evaluate(c[1]))

            elif c[0] == 'для':
                t = []
                if "[" not in c[1]:
                    t.append(self.parser.exp_evaluate(c[1]))
                else:
                    parens = nestedExpr('[', ']')
                    params = parens.parseString("[" + c[1] + "]").asList()[0]
                    for param in params[0]:
                        t.append(self.parser.exp_evaluate(param))
                self.activateTurtle(t)

            elif c[0] == 'повтори':

                i = self.parser.exp_evaluate(c[1])
                while i:
                    self.execute(c[2][1:-1])
                    i -= 1

            elif c[0] == 'пусть':

                if self.parser.current_proc and c[1] in self.parser.current_proc[0]:
                    self.current_proc[0][c[1]] = self.parser.exp_evaluate(c[2])
                else:
                    self.parser.variables[c[1]] = self.parser.exp_evaluate(c[2])

            elif c[0] == 'если':
                if self.parser.exp_evaluate(c[1]):
                    self.execute(c[2][1:-1])

            elif c[0] == 'еслииначе':
                if self.parser.exp_evaluate(c[1]):
                    self.execute(c[2][1:-1])
                else:
                    self.execute(c[3][1:-1])

            elif c[0] == 'иг':
                self.initGraphics()
            elif c[0] == 'ст':
                self.initText()

            elif c[0] == 'нм':
                parens = nestedExpr('[', ']', ignoreExpr=None)
                params = parens.parseString("[" + c[1] + "]").asList()[0]
                self.setTurtlePosition(*list(map(self.parser.exp_evaluate, params[0])))
            elif c[0] == 'новх':
                self.setTurtlePosition(self.parser.exp_evaluate(c[1]), None)
            elif c[0] == 'нову':
                self.setTurtlePosition(None, self.parser.exp_evaluate(c[1]))

            elif c[0] == 'домой':
                self.goHome()

            elif c[0] == 'штамп':
                self.stamp()

            elif c[0] == 'жди':
                wait = float(self.parser.exp_evaluate(c[1])) / 20
                self.app.processEvents()
                time.sleep(wait)

            elif c[0] == 'стоп':
                break

            else:
                raise ValueError("Нет у нас для вас такой команды: " + c[0])

        self.app.processEvents()

        return

    def setPenColor(self, colour):
        for t in self.active:
            t.setTurtleColor(QtGui.QColor(colour))

    def setTextColor(self, colour):
        self.text_colour = QtGui.QColor(colour)

    def setPenState(self, pen_state):
        for t in self.active:
            t.pen_state = pen_state
            t.pen.setStyle(pen_state)

    def setTurtleForm(self, form):
        for t in self.active:
            t.setTurtleForm(QtGui.QPixmap(form))

    def setTurtlePosition(self, x_coord=None, y_coord=None):

        if y_coord:
            y_coord *= -1

        for t in self.active:
            x = t.current_coord[0] if not x_coord else x_coord
            y = t.current_coord[1] if not y_coord else y_coord
            new_coord = [x, y]

            self.scene.addLine(*t.current_coord, *new_coord, pen=t.pen)
            t.current_coord = new_coord
            t.setTurtleAnglePos(t.current_angle)

    def goHome(self):
        init_coord = [[0, 0], [0, 80], [-80, 80], [-80, 0]]
        for i, t in enumerate(self.turtles):
            if t.active:
                self.scene.addLine(*t.current_coord, *init_coord[i], pen=t.pen)
                t.current_coord = init_coord[i]
                t.setTurtleAnglePos(0)

    def draw(self, radius, direction):
        for t in self.active:
            line, t.current_coord = self.form_line(radius, t.current_angle - direction, t.current_coord)
            line = QtCore.QLineF(*line)
            self.scene.addLine(line, pen=t.pen)
            t.setTurtleAnglePos(t.current_angle)

    def turn(self, ang):
        for t in self.active:
            t.setTurtleAnglePos(t.current_angle + ang)

    def activateTurtle(self, turtle):
        for i in range(len(self.turtles)):
            if self.turtles[i].active and i not in turtle:
                self.turtles[i].active = False
            elif i in turtle:
                self.turtles[i].active = True
        self.active = [t for t in self.turtles if t.active]

    def showTurtle(self, show):
        for t in self.active:
            t.show = show
            t.setVisible(t.show)

    def write(self, text):
        txt = MyText()
        txt.setPlainText(text)
        txt.setDefaultTextColor(self.text_colour)
        x, y = self.text_coord
        txt.setPos(x, y)
        self.scene.addItem(txt)
        self.text_coord[1] += 12

    def stamp(self):
        for t in self.active:
            stamp = QtWidgets.QGraphicsPixmapItem(t.pixmap())
            stamp.setTransformOriginPoint(stamp.boundingRect().center())
            stamp.setRotation(t.current_angle)
            stamp.setZValue(1)
            self.scene.addItem(stamp)
            stamp.setPos(t.current_coord[0] - 16, t.current_coord[1] - 16)

    def setBackground(self, color):
        self.scene.setBackgroundBrush(QtGui.QColor(COLORS[color]))

    def floodFill(self, fill=False, pattern=False):

        for t in self.turtles: t.hide()

        t = self.active[0]
        image = QtGui.QImage(self.scene.width(), self.scene.height(), QtGui.QImage.Format_ARGB32)
        image.fill(QtCore.Qt.transparent)

        painter = QtGui.QPainter(image)
        self.scene.render(painter)
        painter.end()

        image_new = QtGui.QImage(image.width(), image.height(), QtGui.QImage.Format_ARGB32)
        image_new.fill(QtCore.Qt.transparent)

        if pattern:
            newColor = QtGui.QColor(QtCore.Qt.darkCyan).rgba()
        elif fill:
            newColor = t.pen_color.rgba()

        x, y = t.current_coord
        x += self.scene.width() / 2
        y += self.scene.height() / 2

        colorToReplace = image.pixel(x, y)
        if newColor == colorToReplace or t.pen_state == QtCore.Qt.NoPen:  # если цвет текущего пикселя равен цвету заливки - ничего не делаем
            for t in self.turtles:
                t.setVisible(t.show)
            return

        toFill = set()
        toFill.add((x, y))

        while len(toFill) != 0:

            x, y = toFill.pop()
            if image.pixel(x, y) != colorToReplace:
                continue
            image.setPixel(x, y, newColor)
            image_new.setPixel(x, y, newColor)

            if x > 0: toFill.add((x - 1, y))
            if x < image.width() - 1: toFill.add((x + 1, y))
            if y > 0: toFill.add((x, y - 1))
            if y < image.height() - 1: toFill.add((x, y + 1))

        if pattern:
            mask = QtGui.QPixmap(image_new).createMaskFromColor(QtGui.QColor(QtCore.Qt.darkCyan), mode=1)
            img = QtGui.QPixmap(image_new)

            if self.scene.backgroundBrush().color() == QtGui.QColor(colorToReplace):
                img.fill(QtCore.Qt.transparent)
            else:
                img.fill(QtGui.QColor(colorToReplace))

            p = QtGui.QPainter(img)
            brush = QtGui.QBrush()
            brush.setTexture(t.form)
            p.setBrush(brush)
            p.fillRect(img.rect(), brush)
            p.end()

            img.setMask(mask)
            image_new = img

            # image_new.save('./render/scene' + str(datetime.now().hour) + 'h' +
            #      str(datetime.now().minute) + 'min' + '.png')

        item = QtWidgets.QGraphicsPixmapItem(QtGui.QPixmap(image_new))
        item.setPos(-self.scene.width() / 2, -self.scene.height() / 2)
        self.scene.addItem(item)
        for t in self.turtles: t.setVisible(t.show)
        return

    def form_line(self, rad, angle, current):

        # считаем последовательные координаты концов линий один за другим
        def polar_to_cartesian(length, ang, cur_x, cur_y):
            x = length * math.cos(math.radians(ang + 270))
            y = length * math.sin(math.radians(ang + 270))
            return [cur_x + x, cur_y + y]

        target = polar_to_cartesian(rad, angle, *current)
        return current + target, target

    def getFieldColor(self):

        for t in self.turtles: t.hide()

        x, y = self.active[0].current_coord
        image = QtGui.QImage(int(self.scene.width()), int(self.scene.height()), QtGui.QImage.Format_ARGB32)
        image.fill(QtCore.Qt.transparent)
        painter = QtGui.QPainter(image)
        self.scene.render(painter)
        painter.end()

        for t in self.turtles: t.setVisible(t.show)

        res = image.pixelColor(320 + x, 190 + y).name()

        for k, v in COLORS.items():
            if res == v:
                return k

    def getTextColor(self):
        for k, v in COLORS.items():
            if self.text_color.name() == v:
                return k

    def getPenColor(self):
        for k, v in COLORS.items():
            if self.active[0].pen_color.name() == v:
                return k

    def getBackgroundColor(self):
        for k, v in COLORS.items():
            if self.scene.backgroundBrush().color().name() == v:
                return k
