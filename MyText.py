
from PyQt5 import QtGui, QtWidgets
from Constnants import COLORS


class MyText(QtWidgets.QGraphicsTextItem):
    def __init__(self, text=""):
        super(MyText, self).__init__()

        self.setFlag(QtWidgets.QGraphicsItem.ItemIgnoresTransformations)
        self.setPlainText(text)
        self.setDefaultTextColor(QtGui.QColor(COLORS[1]))
