# -*- coding: utf-8 -*-

"""TODO:
    - последовтельность аргументов в круглых скобках
    - обработка ошибок: проверка на пробелы и пр.
    - разобраться со знаком "<" в expr_evaluate
    - regex для процедур - убрать \n
    - "покажи" для команд
    – функции для evaluate: остаток, оч,...
    – ошибка в локальных переменных, если совпадает имя переенной при вызове процедуры с локальной переменной
    - закольцевать экран
    - задать управление клавишами
    """

import sys

from PyQt5 import QtCore, QtGui, QtWidgets
from Plotter import Plotter
from ScriptEnter import ScriptEnter
from NewTurtleForm import TurtleFormsWidget
from Constnants import COLORS


class Main(QtWidgets.QMainWindow):
    def __init__(self):
        super(Main, self).__init__()

        self.setGeometry(150, 150, 1050, 410)
        self.setWindowTitle("Рисуем LOGO")
        self.plotter = Plotter(app)
        self.script_enter = ScriptEnter(self.plotter)

        self.central = QtWidgets.QWidget()
        self.central_layout = QtWidgets.QHBoxLayout()
        self.central_layout.addWidget(self.script_enter)
        self.central_layout.addWidget(self.plotter)
        self.central.setLayout(self.central_layout)
        self.setCentralWidget(self.central)
        self.central.adjustSize()

        self.setTopMenu()

        self.show()

    def setTopMenu(self):
        self.menuFile = QtWidgets.QMenu("&File")
        self.actOpen = QtWidgets.QAction("Open", self)
        self.actSave = QtWidgets.QAction("Save", self)

        self.menuForms = QtWidgets.QMenu("&Forms")
        self.actEdit = QtWidgets.QAction("Edit", self)

        self.actOpen.setShortcut(QtGui.QKeySequence.Open)
        self.actSave.setShortcut(QtGui.QKeySequence.Save)

        self.actOpen.triggered.connect(self.on_open)
        self.actSave.triggered.connect(self.on_save)
        self.actEdit.triggered.connect(self.on_edit)

        self.menuFile.addAction(self.actOpen)
        self.menuFile.addAction(self.actSave)
        self.menuForms.addAction(self.actEdit)

        self.menuBar().addMenu(self.menuFile)
        self.menuBar().addMenu(self.menuForms)

    def on_open(self):
        name = QtWidgets.QFileDialog.getOpenFileName(self, 'Open file')[0]
        if name == "":
            return
        file = open(name, 'r')
        with file:
            text = file.read()
            self.script_enter.back.setText(text)
            file.close()

    def on_save(self):
        name = QtWidgets.QFileDialog.getSaveFileName(self, 'Save file')[0]
        if name == "":
            return
        file = open(name, 'w')
        text = self.script_enter.back.toPlainText()
        file.write(text)
        file.close()

    def on_edit(self):
        self.forms = TurtleFormsWidget()
        self.forms.adjustSize()
        self.forms.setWindowTitle("Редактируем черепашку")

        palette = self.forms.palette()
        palette.setColor(QtGui.QPalette.Window, QtGui.QColor(COLORS[0]))
        self.forms.setPalette(palette)

        desktop = QtWidgets.QApplication.desktop()
        x = (desktop.width() - self.forms.width()) // 2
        y = (desktop.height() - self.forms.height()) // 2
        self.forms.move(x, y)

        self.forms.show()

    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_Escape:
            self.close()


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    main = Main()
    sys.exit(app.exec_())
