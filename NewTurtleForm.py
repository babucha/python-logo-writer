
import os
from PyQt5 import QtCore, QtGui, QtWidgets
from Constnants import COLORS

t_catalog = './turtles/'
FORMS = sorted([i.name for i in os.scandir(t_catalog) if i.name.endswith('.png')],
               key=lambda x: int(x[0:~3]))


class TurtleFormEditor(QtWidgets.QGraphicsView):
    close = QtCore.pyqtSignal()

    def __init__(self, t, image):
        super(TurtleFormEditor, self).__init__()

        self.setWindowTitle('Редактироание черепашки')

        self.resize(540, 530)

        self.scene = QtWidgets.QGraphicsScene()
        self.setScene(self.scene)
        self.scene.setBackgroundBrush(QtGui.QColor(COLORS[0]))

        self.pixels = {}
        self.changed = set()

        self.initial_form = QtGui.QImage(image)
        self.name = str(t + 1)
        self.preview = QtWidgets.QGraphicsPixmapItem(QtGui.QPixmap(self.initial_form))

        self.initForm()
        self.show()

    def initForm(self):

        for i in range(32):
            for j in range(32):
                pixel = Pixel()
                pixel.setPos(i * 11, j * 11)
                self.scene.addItem(pixel)
                self.pixels[(i, j)] = pixel
                if self.initial_form.pixelColor(i, j) != QtCore.Qt.transparent:
                    pixel.reversePixel()

        self.preview.setPos(self.scene.width() + 50, self.scene.height() / 2 - 20, )
        self.scene.addItem(self.preview)

        self.updateForm()

    def updateForm(self):
        for k, v in self.pixels.items():
            if v.status:
                self.initial_form.setPixelColor(*k, QtGui.QColor(COLORS[4]))
            else:
                self.initial_form.setPixelColor(*k, QtCore.Qt.transparent)

        self.preview.setPixmap(QtGui.QPixmap(self.initial_form))

    def mousePressEvent(self, event):
        if isinstance(self.itemAt(event.pos()), Pixel):
            self.itemAt(event.pos()).reversePixel()
            self.changed.add(self.itemAt(event.pos()))
        self.updateForm()

    def mouseMoveEvent(self, event):
        if isinstance(self.itemAt(event.pos()), Pixel) and self.itemAt(event.pos()) not in self.changed:
            self.itemAt(event.pos()).reversePixel()
            self.changed.add(self.itemAt(event.pos()))
            self.updateForm()

    def mouseReleaseEvent(self, event):
        self.changed.clear()

    def closeEvent(self, event):
        self.initial_form.save('./turtles/' + self.name + '.png')
        self.close.emit()


class FormLabel(QtWidgets.QLabel):
    clicked = QtCore.pyqtSignal()

    def __init__(self):
        super(FormLabel, self).__init__()

    def mousePressEvent(self, e):
        QtWidgets.QLabel.mousePressEvent(self, e)
        self.clicked.emit()


class TurtleFormsWidget(QtWidgets.QWidget):
    def __init__(self):
        super(TurtleFormsWidget, self).__init__()
        self.setWindowTitle("Редактор форм")

        self.palette().setColor(QtGui.QPalette.Window, QtGui.QColor(COLORS[0]))
        self.resize(500, 500)

        self.grid = QtWidgets.QGridLayout()
        self.setLayout(self.grid)

        self.signalMapper = QtCore.QSignalMapper()
        self.signalMapper.mapped.connect(self.startEditor)

        self.setForms()
        self.show()

        # for i in range(40,90):
        #     QtGui.QPixmap('./turtles/5.png').save('./turtles/' + str(i) + '.png')

    def setForms(self):

        self.forms = []
        n = 0
        for i in range(0, len(FORMS)):
            for j in range(0, 6):

                hbox = QtWidgets.QHBoxLayout()
                image = QtGui.QPixmap(t_catalog + FORMS[n]).scaled(48, 48)

                form_image = FormLabel()
                form_image.setPixmap(image)

                form_number = FormLabel()
                form_number.setText(str(n + 1) + '.')
                form_number.setAlignment(QtCore.Qt.AlignCenter)
                form_number.setStyleSheet("color: #ff0000")

                hbox.addWidget(form_number)
                hbox.addWidget(form_image)

                form_image.clicked.connect(self.signalMapper.map)
                self.signalMapper.setMapping(form_image, n)
                form_number.clicked.connect(self.signalMapper.map)
                self.signalMapper.setMapping(form_number, n)

                self.forms.append(form_image)

                self.grid.addLayout(hbox, i, j)

                n += 1
                if n >= len(FORMS): break
            if n >= len(FORMS): break

    def startEditor(self, n):
        self.hide()
        self.editor = TurtleFormEditor(n, self.forms[n].pixmap().scaled(32, 32))
        self.editor.close.connect(self.clearGrid)

    def clearGrid(self):
        for i in range(self.grid.count()):
            for j in range(0, 2):
                self.grid.itemAt(i).itemAt(j).widget().close()
        self.show()
        self.setForms()

    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_Escape:
            self.close()


class Pixel(QtWidgets.QGraphicsRectItem):
    def __init__(self):
        super(Pixel, self).__init__()

        self.status = False

        self.brush = QtGui.QBrush(QtGui.QColor(COLORS[0]))
        self.pen = QtGui.QPen(QtGui.QColor(COLORS[1]))
        self.setPen(self.pen)
        self.setBrush(self.brush)
        self.setRect(0, 0, 10, 10)

    def reversePixel(self):
        self.status = not self.status
        self.brush.setColor(QtGui.QColor(COLORS[1])) if self.status \
            else self.brush.setColor(QtGui.QColor(COLORS[0]))
        self.setBrush(self.brush)
