COLORS = {
    0: "#000000",  # "BLACK"       #000000
    1: "#ffffff",  # "WHITE"       #ffffff
    2: "#6a5acd",  # "SLATE BLUE"  #6a5acd
    3: "#ee82ee",  # "VIOLET"      #ee82ee
    4: "#ff0000",  # "RED"         #ff0000
    5: "#191970",  # "MIDNIGHT BLUE" #191970
    6: "#ffa500",  # "ORANGE"      #ffa500
    7: "#008000",  # "GREEN"       #008000
    8: "#a9a9a9",  # "DARK GRAY"   #a9a9a9
    9: "#808080",  # "GRAY"        #808080
    10: "#87ceeb",  # "SKY BLUE"   #87ceeb
    11: "#ffc0cb",  # "PINK"       #ffc0cb
    12: "#ff4500",  # "ORANGE RED" #ff4500
    13: "#0000cd",  # "MEDIUM BLUE" #0000cd
    14: "#ffff00",  # "YELLOW"     #ffff00
    15: "#00ff7f",  # "SPRING GREEN" #00ff7f
}

LOGO_COMMANDS = 'повтори вп нд лв пр нц нцт по пп штамп новфон нф ' \
                'крась сч пч пусть покажи пиши для если еслииначе ' \
                'иг ст нм новх нову домой жди стоп заполни новкурс нк '

UNARY = 'сл оч целое кк cos sin arctg '

BINARY = 'остаток доточки угол '

SENSOR = 'х у место курс цвет цп цт фон '

LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"

DIGITS = "0123456789"
